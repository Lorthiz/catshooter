﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemy;

    // Use this for initialization
    void Start() {
        InvokeRepeating("Spawn", 1f, 1f);
    }

    // Update is called once per frame
    void Update() {
        
    }

    void Spawn() {
        float x = Screen.width/4;
        float y = Screen.height/4;
        Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(x, x *3 ), Random.Range(y, y*3), Camera.main.farClipPlane / 2));
        Object.Instantiate(enemy, screenPosition, Quaternion.identity);
    }
}
