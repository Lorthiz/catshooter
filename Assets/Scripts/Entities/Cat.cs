﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour, Entity {

    public int hp;
    public Sprite dead, hit, deadOnGround;
    private Rigidbody2D rigidbody;
    private CircleCollider2D collider;
    private SpriteRenderer spriteRenderer;
    private State state;
    private int velocity;
    private enum State {
        ALIVE, DEAD, DEADONGROUND
    };

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<CircleCollider2D>();
        rigidbody.freezeRotation = true;
        velocity = Random.Range(1,3);
        if (Random.value >= 0.5) {
            velocity = -velocity;
        }
        state = State.ALIVE;
        
	}
	
	// Update is called once per frame
	void Update () {
        rigidbody.AddForce(new Vector3(velocity, 1, 0));
	}
    
    public void GetHit(int damage) {
        hp -= 1;
        if (hp <= 0 && state != State.DEAD && state != State.DEADONGROUND) {
            rigidbody.gravityScale = 1;
            spriteRenderer.sprite = dead;
            state = State.DEAD;
        }

        
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (state == State.DEAD && coll.gameObject.name== "Bottom") {
            spriteRenderer.sprite = deadOnGround;
            state = State.DEADONGROUND;
            rigidbody.simulated = false;
            rigidbody.isKinematic = false;
            collider.enabled = false;
            Destroy(gameObject, 5);
        }
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
