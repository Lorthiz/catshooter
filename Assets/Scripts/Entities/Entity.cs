﻿using UnityEngine;
using System.Collections;

public interface Entity {

    void GetHit(int damage);
}
