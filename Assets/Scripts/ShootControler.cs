﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootControler : MonoBehaviour {

    AudioSource audioSource;
    public int magazineSize;
    private int ammoCount;

    public AudioClip shoot;
    public AudioClip crock;
    public AudioClip empty;
    public AudioClip reload;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        ammoCount = magazineSize;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Fire1")) {
            Shoot();
        }
        if (Input.GetButtonDown("Fire2")) {
            Reload();
        }
	}


    private void Shoot() {
        if (ammoCount > 0) {
            audioSource.PlayOneShot(shoot);
            --ammoCount;
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider == null)
                return;
            HitEntity(hit);
        } else {
            audioSource.PlayOneShot(empty);
        }
    }

    private void Reload() {
        if (ammoCount < magazineSize) {
            audioSource.PlayOneShot(reload);
            ammoCount = magazineSize;
        } else {
            audioSource.PlayOneShot(crock);
        }
    }

    private void HitEntity(RaycastHit2D hit) {
        Entity e = hit.collider.gameObject.GetComponent<Entity>();
        if (e != null) {
            e.GetHit(1);
        }
    }
}